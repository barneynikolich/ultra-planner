import {AfterViewInit , Component , ElementRef , HostListener , OnInit , ViewChild} from '@angular/core';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.css']
})
export class CalendarViewComponent implements OnInit, AfterViewInit {

  public screenHeight: number;
  public screenWidth: number;
  public calendarDivHeight: number;

  @ViewChild('divElement')
  public calendarDiv: ElementRef;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight - 160;
    this.screenWidth = window.innerWidth;
    this.calendarDivHeight = (<HTMLDivElement>this.calendarDiv.nativeElement).offsetHeight;
  }

  constructor() {
    this.screenHeight = window.innerHeight - 160;
    this.screenWidth = window.innerWidth;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.calendarDivHeight = (<HTMLDivElement>this.calendarDiv.nativeElement).offsetHeight;
  }

  public calculateSingleBoxWidth(): number {
    return this.seventyFivePercentScreenWidth / 8;
  }

  public calculateSingleBoxHeight(): number {
    const weeksInMonth = this.getDaysInMonth().length / 7;
    console.log(this.screenHeight)
    const heightForSingleBox = ((this.screenHeight - 200) / weeksInMonth);
    // const heightForSingleBox = (window.innerHeight - 160) / weeksInMonth;
    return heightForSingleBox;
  }

  public getDaysInMonth(): number[] {
    const d = new Date();
    const year = d.getFullYear();
    const month = d.getMonth() + 1;
    return Array(new Date(year, month, 0).getDate());
  }

  get seventyFivePercentScreenWidth(): number {
    return ((this.screenWidth / 100) * 75);
  }
}
