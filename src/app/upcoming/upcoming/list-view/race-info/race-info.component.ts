import {Component , Input , OnInit} from '@angular/core';
import {Race} from '../../../../models/races';

@Component({
  selector: 'app-race-info',
  templateUrl: './race-info.component.html',
  styleUrls: ['./race-info.component.css']
})
export class RaceInfoComponent implements OnInit {

  @Input()
  public race: Race;

  constructor() { }

  ngOnInit() {
  }

}
