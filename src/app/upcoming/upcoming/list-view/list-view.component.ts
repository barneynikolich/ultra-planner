import {Component , ElementRef , Input , ViewChild} from '@angular/core';
import { Race } from 'src/app/models/races';
import {slideUpDown} from '../../../animations';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css'],
  animations:  [slideUpDown]
})
export class ListViewComponent {

  @Input()
  public races: Race[];

  @Input()
  public month: string;

  @Input()
  public showSpinner;

  @Input()
  percentageWidth: number;

  @ViewChild('listViewElement')
  public listViewContainerDiv: ElementRef;

  constructor() { }

  public getRaceDay(race: Race): string {
    return race.date.split(' ')[0];
  }

  public getRaceDate(race: Race): string {
      return race.date.split(' ')[1];
  }

  public getRaceMonth(race: Race): string {
      return this.month.substr(0, 3);
  }

  public toggleRaceDetail(race: Race): void {
    race.showRaceDetail = !race.showRaceDetail;
  }

  public getWidthOfParent(): number {
    // Calculate width of the parent container then calculate the percentage width for each race to fill
    const width: number = (<HTMLDivElement>this.listViewContainerDiv.nativeElement).offsetWidth;
    return width * (this.percentageWidth / 100);
  }
}
