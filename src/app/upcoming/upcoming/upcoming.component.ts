import {  Component, OnInit } from '@angular/core';
import { RaceService } from 'src/app/services/race.service';
import { Race } from 'src/app/models/races';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {

  public showCalendar: boolean;
  public showSpinner = false;

  private _upcomingRaces: Race[];

  constructor(private _raceService: RaceService) {}

  public ngOnInit(): void {
    this.showSpinner = true;
    this._raceService.getUpcomingRaces().subscribe((races: Race[]) => {
      this._upcomingRaces = races;
      this.showSpinner = false;
    });
  }

  public toggleView(): void {
    this.showCalendar = !this.showCalendar;
  }

  get races(): Race[] {
    return this._upcomingRaces;
  }

  get title(): string {
    return 'Upcoming races in ' + this.currentMonth;
  }

  get currentMonth(): string {
    const monthNames = [
      'January' , 'February' , 'March' ,
      'April' , 'May' , 'June' ,
      'July' , 'August' , 'September' ,
      'October' , 'November' , 'December'
    ];
    return monthNames[new Date().getMonth()];
  }

}
