import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpcomingComponent } from './upcoming.component';
import { NavigationModule } from 'src/app/navigation/navigation.module';
import { NgMaterialModule } from 'src/app/ng-material/ng-material.module';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { RaceInfoComponent } from './list-view/race-info/race-info.component';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [UpcomingComponent, CalendarViewComponent, ListViewComponent, RaceInfoComponent],
  imports: [
    CommonModule,
    NavigationModule,
    NgMaterialModule,
    FlexLayoutModule
  ],
  exports: [ListViewComponent]
})
export class UpcomingModule { }
