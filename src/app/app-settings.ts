import {environment} from '../environments/environment';

export class AppSettings {
    // TODO: Place all urls in here
    // public static baseUrl: string = 'http://localhost:8080/api/v1/';

  private static baseUrl: string = environment.localhost;


  public static upcomingRaces: string = `${AppSettings.baseUrl}races/upcoming`;
  public static findAllRaces: string = `${AppSettings.baseUrl}races/`;
  public static terrains: string = `${AppSettings.baseUrl}/terrains`;
  public static locations: string = `${AppSettings.baseUrl}/locations`;

  public static IPAD_WIDTH: number = 1024;
  
}
