import { Injectable } from '@angular/core';
import {HttpClient , HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../app-settings';
import { Race } from '../models/races';
import {Filters} from '../race-search/race-search/race-filter/models/filters';

@Injectable({
  providedIn: 'root'
})
export class RaceService {

  constructor(private _httpClient: HttpClient) { }

  public getUpcomingRaces(): Observable<object> {
    console.log('firing load')
    return this._httpClient.get(AppSettings.upcomingRaces, {});
  }


  public findAll(): Observable<Race[]> {
    return this._httpClient.get<Race[]>(AppSettings.findAllRaces, {});
  }

  public findAllWithFilters(filter: Filters): Observable<Race[]> {

    let params = new HttpParams();
    if (filter) {
      for (const key in filter.filters) {
        params = params.append(key, filter.filters[key]);
      }
    }

    return this._httpClient.get<Race[]>(AppSettings.findAllRaces, {params: params});
  }

  public getTerrains(): Observable<Object> {
    return this._httpClient.get(AppSettings.terrains);
  }

  public getLocations(): Observable<Object> {
    return this._httpClient.get(AppSettings.locations);
  }
}
