import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {

  @Input()
  public marginTop: number = 50;

  @Input()
  public title: string;

  @Input()
  public height: number = 100;

  @Input()
  public homePage: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
