import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';
import { HeaderComponent } from './header/header.component';
import { SubHeaderComponent } from './header/sub-header/sub-header.component';
import { NgMaterialModule } from '../ng-material/ng-material.module';
import { RoutingModule } from '../routing/routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    SidenavListComponent,
    HeaderComponent,
    SubHeaderComponent
  ],
  imports: [
    CommonModule,
    NgMaterialModule,
    RoutingModule,
    FlexLayoutModule
  ],
  exports: [
    SubHeaderComponent,
    SidenavListComponent,
    HeaderComponent
  ]
})
export class NavigationModule { }
