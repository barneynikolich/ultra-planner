import {animate , group , state , style , transition , trigger} from '@angular/animations';


export const flyInLeftFlyOutRight = trigger('EnterLeave', [
    transition(':enter', [
      style({ transform: 'translateX(-100%)' }),
      animate('0.3s 0.2s ease-in')
    ]),
    transition(':leave', [
      animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
    ])
]);

export const fadeInFadeOut =  trigger('EnterLeave', [
  transition(':enter', [
    style({  transform: 'scale(0.0)' }), animate('0.3s')
  ]),
  transition(':leave', [
    animate('0.3s ease-out', style({ transform: 'scale(0.0)' }))
  ])
]);

export const slideUpDown =
  trigger('slideInOut', [
    state('in', style({height: '*', opacity: 0})),
    transition(':leave', [
      style({height: '*', opacity: 1}),
      group([
        animate(200, style({height: 0})),
        animate('200ms ease-in-out', style({'opacity': '0'}))
      ])
    ]),
    transition(':enter', [
      style({height: '0', opacity: 1, overflow: 'hidden'}),
      group([
        animate(200, style({height: '*'}))
      ])

    ])
  ]);


