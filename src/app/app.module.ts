import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoutingModule } from './routing/routing.module';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import {NgMaterialModule} from './ng-material/ng-material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { UpcomingModule } from './upcoming/upcoming/upcoming.module';
import { NavigationModule } from './navigation/navigation.module';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about/about.component';
import {ContactModule} from './contact/contact.module';
import {RaceSearchModule} from './race-search/race-search.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    FooterComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    NgMaterialModule,
    BrowserAnimationsModule,
    UpcomingModule,
    NavigationModule,
    FlexLayoutModule,
    HttpClientModule,
    ContactModule,
    RaceSearchModule
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
