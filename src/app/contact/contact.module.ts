import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import {NgMaterialModule} from '../ng-material/ng-material.module';
import {NavigationModule} from '../navigation/navigation.module';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    NgMaterialModule,
    NavigationModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: []
})
export class ContactModule { }
