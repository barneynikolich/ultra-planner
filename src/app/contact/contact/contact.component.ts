import { Component, OnInit } from '@angular/core';
import {FormBuilder , FormControl , FormGroup , FormGroupDirective , NgForm , Validators} from '@angular/forms';
import {ContactRequest} from '../models/contact-request.modal';
import {ErrorStateMatcher} from '@angular/material';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  public title = 'Contact';

  public contactMeForm: FormGroup;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  firstNameFormControl = new FormControl('', [
    Validators.required,
  ]);

  lastNameFormControl = new FormControl('', [
    Validators.required,
  ]);

  matcher = new MyErrorStateMatcher();


  constructor( private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createContactMeForm();
  }

  public onSubmit(modal: ContactRequest): void {
    console.log('Submitted!')
    const contactRequest: ContactRequest = new ContactRequest(modal.firstName, modal.lastName, modal.email, modal.message, new Date());

    this.resetContactForm();
  }

  private createContactMeForm(): void {
    this.contactMeForm = this._formBuilder.group({
      firstName: this.firstNameFormControl,
      lastName: this.lastNameFormControl,
      email: this.emailFormControl,
      message: ''
    });
  }

  private resetContactForm(): void {
    this.contactMeForm.reset();
  }

}
