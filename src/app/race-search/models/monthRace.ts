import { Race } from 'src/app/models/races';

export class MonthRace {
  private _monthRace: object = {};

  constructor() {
  }

  public toggleRace(month: string, race: Race): void {
    this.toggleColumnKey(this.findColumnKeys(month), race);
  }

  public findColumnKeys(month: string): Race[] {
    let columnKeys = this._monthRace[month];
    if (columnKeys) {
      return columnKeys;
    }
    columnKeys = [];
    this._monthRace[month] = columnKeys;
    return columnKeys;
  }

  private toggleColumnKey(columnKeys: Race[], race: Race): void {
    const index = columnKeys.indexOf(race);
    if (index === -1) {
      columnKeys.push(race);
    } else {
      columnKeys.splice(index, 1);
    }
  }

  public reset(): void {
    this._monthRace = [];
  }

  get filters(): object {
    return this._monthRace;
  }
}
