import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RaceSearchComponent } from './race-search/race-search.component';
import { MonthViewComponent } from './race-search/month-view/month-view.component';
import {NavigationModule} from '../navigation/navigation.module';
import {NgMaterialModule} from '../ng-material/ng-material.module';
import {UpcomingModule} from '../upcoming/upcoming/upcoming.module';
import { RaceFilterComponent } from './race-search/race-filter/race-filter.component';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [RaceSearchComponent, MonthViewComponent, RaceFilterComponent],
  imports: [
    CommonModule,
    NavigationModule,
    NgMaterialModule,
    UpcomingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RaceSearchModule { }
