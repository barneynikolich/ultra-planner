import { Component, OnInit, ViewChild } from '@angular/core';
import { RaceService } from 'src/app/services/race.service';
import { Race } from 'src/app/models/races';
import { MonthRace } from '../models/monthRace';
import { Month } from '../models/Month';
import {Filters} from './race-filter/models/filters';
import { RaceFilterComponent } from './race-filter/race-filter.component';

@Component({
  selector: 'app-race-search',
  templateUrl: './race-search.component.html',
  styleUrls: ['./race-search.component.css']
})
export class RaceSearchComponent implements OnInit {
  public static readonly MONTHS = [Month.JANUARY, Month.FEBRUARY, Month.MARCH, Month.APRIL, Month.MAY, Month.JUNE, Month.JULY,
    Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER, Month.DECEMBER];

  public title = 'Search races';

  public races: Race[];

  public _locations: string[];

  public _terrains: string[];

  public _raceMonthMap: MonthRace = new MonthRace();

  public showMonthView: boolean;

  public filteredRaces: Race[] = [];

  public filteredRaceCount: number;

  public sideNavOpen = true;

  @ViewChild(RaceFilterComponent) raceFilterComponent: RaceFilterComponent;

  constructor(private _raceService: RaceService) { }

  ngOnInit() {
    this._raceService.findAll().subscribe((races: Race[]) => {
      this.races = races;
      this._raceMonthMap = this.populateMonthRaces(this.races);
    });
    this._raceService.getTerrains().subscribe((terrains: string[]) => {
      this._terrains = terrains;
    });
    this._raceService.getLocations().subscribe((locations: string[]) => {
      this._locations = locations;
    });
  }

  public showFilter() {
    this.raceFilterComponent.sideNav.toggle();
  }

  public onSideNavChange(event): void {
    this.sideNavOpen = event;
  }

  public onFilterRaces(filters: Filters): void {
    this._raceService.findAllWithFilters(filters).subscribe((races: Race[]) => {
      if (filters.getRaceCount) {
        this.filteredRaceCount = races.length;
      } else {
        this.filteredRaces = races;
      }
    });
  }

  public populateMonthRaces(races: Race[]): MonthRace {
    const raceMonthMap: MonthRace = new MonthRace();

    RaceSearchComponent.MONTHS.forEach((month: string) => {
      races.forEach((race: Race) => {

        const monthShort = month.slice(0, 3);
        const raceDateMonthShort: string = race.date.slice(7, 10);
        if (monthShort === raceDateMonthShort) {
          raceMonthMap.toggleRace(month, race);
        }
      });
    });
    return raceMonthMap;
  }

  public toggleView(): void {
    this.showMonthView = !this.showMonthView;
  }



}
