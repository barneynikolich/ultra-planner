export class DistanceOptions {

  // public static readonly lessThan26 = '< 26';
  public static readonly Twenty_Six_to_49 = '26-49';
  public static readonly Fifty_to_69 = '50-69';
  public static readonly Seventy_to_99 = '70-99';
  public static readonly Greater_100 = '>100';

}
