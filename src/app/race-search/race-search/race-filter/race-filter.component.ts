import {Component , EventEmitter , Input , OnInit , Output, ViewChild, ElementRef} from '@angular/core';
import {FormBuilder , FormGroup , Validators} from '@angular/forms';
import {DistanceOptions} from './models/distanceOptions';
import {Filters} from './models/filters';
import {TableFilterLogic} from './models/table-filter-logic';
import {Race} from '../../../models/races';
import { MatSidenav } from '@angular/material';
import { AppSettings } from 'src/app/app-settings';

@Component({
  selector: 'app-race-filter',
  templateUrl: './race-filter.component.html',
  styleUrls: ['./race-filter.component.css']
})
export class RaceFilterComponent implements OnInit {

  public distanceOptions = [DistanceOptions.Twenty_Six_to_49, DistanceOptions.Fifty_to_69,
    DistanceOptions.Seventy_to_99, DistanceOptions.Greater_100];

  @Input()
  public filteredRaces: Race[];

  @Input()
  public locations: string[];

  @Input()
  public terrains: string[];

  @Input()
  public resultCount = 10;

  public filterRacesForm: FormGroup;

  @Output()
  public onFilterRaces = new EventEmitter<Filters>();

  @Output()
  public onResetRacesAndCount = new EventEmitter<boolean>();

  @Output()
  public onSideNavOpen = new EventEmitter<boolean>();

  @ViewChild('sidenav') sideNav: MatSidenav;

  public sideNavOpen = true;

  private _filters: Filters = new Filters();


  selectedLocation: string;
  selectedTerrain: string;


  constructor(private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.createFilterRaceForm();
  }

  public getResultCount(model: TableFilterLogic) {
    // console.log('Change deteced', model)
    if (this.formIsNotEmpty()) {
      this.setFiltersFromModel(model, true);
      this.onFilterRaces.emit(this._filters);
    } else {
      this.onResetRacesAndCount.emit();
    }
  }

  public filterRacesSubmit(model: TableFilterLogic) {
    if (this.formIsNotEmpty()) {
      this.setFiltersFromModel(model, false);
      this.onFilterRaces.emit(this._filters);

      if (window.innerWidth <= AppSettings.IPAD_WIDTH)
      {
        this.sideNav.toggle();
        console.log('Should close')
      }
      console.log('shouldnt close')
    } else {
      this.onResetRacesAndCount.emit(true);
    }
  }

  public resetFilterForm(): void {
    this.filterRacesForm.setValue({
      distance: '',
      area: '',
      terrain: '',
      dateSort: ''
    });
  }

  public toggleArrow(event): void {
    this.sideNavOpen = event;
    this.onSideNavOpen.emit(event);
  }

  private setFiltersFromModel(model: TableFilterLogic, raceCount: boolean): void {
    this._filters.reset();
    this._filters.addFilter('distance', model.distance);
    this._filters.addFilter('area', model.area);
    this._filters.addFilter('terrain', model.terrain);
    this._filters.addFilter('dateSort', model.dateSort);
    this._filters.getRaceCount = raceCount;
  }


  private formIsNotEmpty(): boolean {
    return this.filterRacesForm.get('distance').value !== '' || this.filterRacesForm.get('area').value !== ''
      || this.filterRacesForm.get('terrain').value !== '' || this.filterRacesForm.get('dateSort').value !== '';
  }

  private createFilterRaceForm() {
    this.filterRacesForm = this.formBuilder.group({
      distance:  [''],
      area: [''],
      terrain: [''],
      dateSort: ['']
    });
  }

}
