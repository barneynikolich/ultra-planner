import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceFilterComponent } from './race-filter.component';

describe('RaceFilterComponent', () => {
  let component: RaceFilterComponent;
  let fixture: ComponentFixture<RaceFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaceFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
