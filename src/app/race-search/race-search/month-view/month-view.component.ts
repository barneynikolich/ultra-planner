import {Component, Input} from '@angular/core';
import {Month} from '../../models/Month';
import {Race} from 'src/app/models/races';
import {MonthRace} from '../../models/monthRace';
import {slideUpDown} from '../../../animations';

@Component({
  selector: 'app-month-view' ,
  templateUrl: './month-view.component.html' ,
  styleUrls: ['./month-view.component.css'] ,
  animations: [slideUpDown]
})
export class MonthViewComponent {

  public static readonly MONTHS = [Month.JANUARY , Month.FEBRUARY , Month.MARCH , Month.APRIL , Month.MAY , Month.JUNE , Month.JULY ,
    Month.AUGUST , Month.SEPTEMBER , Month.OCTOBER , Month.NOVEMBER , Month.DECEMBER];

  @Input()
  public races: Race[] = [];

  @Input()
  public raceMonths: MonthRace;

  private _selectedMonth: string[] = [];

  public toggleMonthDropdown(month: string): void {
    const index: number = this._selectedMonth.indexOf(month);
    if (index === -1) {
      this._selectedMonth.push(month);
    } else {
      this._selectedMonth.splice(index , 1);
    }
  }

  public showMonthDropdown(month: string): boolean {
    return this._selectedMonth.some(x => x === month);
  }

  public isActive(month: string): boolean {
    return this._selectedMonth.some(x => x === month);
  }

  get months(): string[] {
    return MonthViewComponent.MONTHS;
  }

  get showSpinner(): boolean {
    return this.races === undefined;
  }

}
