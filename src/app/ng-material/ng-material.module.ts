import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule ,
  MatIconModule ,
  MatListModule ,
  MatMenuModule ,
  MatSidenavModule ,
  MatTabsModule ,
  MatToolbarModule ,
  MatDividerModule ,
  MatGridListModule ,
  MatCardModule , MatTooltipModule , MatOptionModule , MatSelectModule , MatProgressSpinnerModule , MatInputModule , MatFormFieldModule
} from '@angular/material';

@NgModule({
  imports: [
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    CommonModule,
    MatTabsModule,
    MatSidenavModule,
    MatDividerModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatOptionModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule
  ],
  exports: [
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatCardModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatFormFieldModule,
    MatSidenavModule
  ],
  declarations: []
})
export class NgMaterialModule { }
