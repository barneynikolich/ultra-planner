import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from '../home/home.component';
import { UpcomingComponent } from '../upcoming/upcoming/upcoming.component';
import { AboutComponent } from '../about/about.component';
import {ContactComponent} from '../contact/contact/contact.component';
import {RaceSearchComponent} from '../race-search/race-search/race-search.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'upcoming', component: UpcomingComponent },
  { path: 'search', component: RaceSearchComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
